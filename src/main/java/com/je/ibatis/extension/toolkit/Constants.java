/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.toolkit;

/**
 * Constants
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/25
 */
public class Constants {

    /**
     * 英文点(.)
     */
    public static final String DOT = ".";

    /**
     * 空格
     */
    public static final String SPACE = " ";
    /**
     * 特殊属性：表编码
     */
    public static final String KEY_TABLE_CODE = "$TABLE_CODE$";
    /**
     * 特殊属性：功能编码
     */
    public static final String KEY_FUNCTION_CODE = "$FUNCTION_CODE$";
    /**
     * 特殊属性：功能编码
     */
    public static final String KEY_SELECT_COLUMN = "__SELECT_COLUMNS";
    /**
     * 主键值
     */
    public static final String KEY_PK_VALUE = "__PK_VALUE";
    /**
     * 基础元数据Mapper的命名空间
     */
    public static final String BASE_MAPPER_NAMESPACE = "!MetaBaseMapper";
    /**
     * 基础元数据Mapper的位置
     */
    public static final String BASE_MAPPER_LOCATION = "com/je/ibatis/extension/mapper/MetaBaseMapper.xml";
    /**
     * 表缓存
     */
    public static final String CACHE_KEY_TABLE = "!MetaTableCache";
    /**
     * 功能缓存
     */
    public static final String CACHE_KEY_FUNCTION = "!MetaFunctionCache";
    /**
     * sql缓存
     */
    public static final String CACHE_KEY_SQL_NODE = "!MetaSqlNodeCache";
    /**
     * 自定义sql别名
     */
    public static final String SQL_ALIAS = "__sql";
    /**
     * Map别名
     */
    public static final String MAP_ALIAS = "map";
    /**
     * Map中参数别名
     */
    public static final String MAP_PARAMETER = MAP_ALIAS + ".%s";
    /**
     * Map中的表编码
     */
    public static final String KEY_MAP_TABLE_CODE = String.format(MAP_PARAMETER, KEY_TABLE_CODE);
    /**
     * List别名
     */
    public static final String LIST_ALIAS = "list";
    /**
     * page别名
     */
    public static final String PAGE_ALIAS = "page";
    /**
     * ConditionsWrapper别名
     */
    public static final String WRAPPER_ALIAS = "wrapper";
    /**
     * 参数别名
     */
    public static final String WRAPPER_PARAMETER = "#{%s.parameter.%s}";
    /**
     * 参数别名前缀
     */
    public static final String WRAPPER_PARAMETER_PREFIX = WRAPPER_ALIAS + ".parameter.";
    /**
     * 特殊属性：条件查询器中的表编码
     */
    public static final String KEY_WRAPPER_TABLE_CODE = String.format("%s.parameter.%s", WRAPPER_ALIAS, KEY_TABLE_CODE);
    /**
     * 特殊属性：条件查询器中的功能编码
     */
    public static final String KEY_WRAPPER_FUNCTION_CODE = String.format("%s.parameter.%s", WRAPPER_ALIAS, KEY_FUNCTION_CODE);
    /**
     * 特殊属性：条件查询器中的加载列
     */
    public static final String KEY_WRAPPER_SELECT_COLUMN = String.format("%s.parameter.%s", WRAPPER_ALIAS, KEY_SELECT_COLUMN);


}