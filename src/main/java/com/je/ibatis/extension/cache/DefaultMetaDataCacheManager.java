/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.cache;

import com.je.ibatis.extension.metadata.model.Function;
import com.je.ibatis.extension.metadata.model.Table;
import com.je.ibatis.session.CustomConfiguration;

/**
 * MetaDataCacheManager
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/22
 */
public class DefaultMetaDataCacheManager implements MetaDataCacheManager {

    protected CustomConfiguration configuration;
    protected final MetaFunctionCache metaFunctionCache;
    protected final MetaTableCache metaTableCache;
    protected final MetaSqlNodeCache metaSqlNodeCache;

    public DefaultMetaDataCacheManager() {
        metaFunctionCache = MetaFunctionCache.INSTANCE;
        metaTableCache = MetaTableCache.INSTANCE;
        metaSqlNodeCache = MetaSqlNodeCache.INSTANCE;
    }

    public DefaultMetaDataCacheManager(CustomConfiguration configuration) {
        this.configuration = configuration;
        metaFunctionCache = MetaFunctionCache.INSTANCE;
        metaTableCache = MetaTableCache.INSTANCE;
        metaSqlNodeCache = MetaSqlNodeCache.INSTANCE;
    }

    @Override
    public void put(Function func) {
        metaFunctionCache.putObject(func.getCode(), func);
    }

    @Override
    public void put(Table table) {
        metaTableCache.putObject(table.getCode(), table);
    }

    @Override
    public void removeFunction(String code) {
        metaFunctionCache.removeObject(code);
    }

    @Override
    public void removeTable(String code) {
        metaTableCache.removeObject(code);
    }

    @Override
    public Function getFunction(String code) {
        Object object = metaFunctionCache.getObject(code);
        if (object == null) {
            return null;
        }
        return (Function) object;
    }

    @Override
    public Table getTable(String code) {
        Object object = metaTableCache.getObject(code);
        if(object==null){
            object= metaTableCache.getObject(code.toUpperCase());
        }
        if(object==null){
            object= metaTableCache.getObject(code.toLowerCase());
        }
        if (object == null) {
            return null;
        }
        return (Table) object;
    }

    @Override
    public void clear() {
        metaFunctionCache.clear();
        metaTableCache.clear();
        metaSqlNodeCache.clear();
    }

    @Override
    public void clearFunction() {
        metaFunctionCache.clear();
    }

    @Override
    public void clearTable() {
        metaTableCache.clear();
    }

    public CustomConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public void setConfiguration(CustomConfiguration configuration) {
        this.configuration = configuration;
    }
}