/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.cache;

import com.je.ibatis.extension.toolkit.Constants;
import org.apache.ibatis.cache.Cache;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Sql缓存数据
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/22
 */
public class MetaSqlNodeCache implements Cache {

    protected static final MetaSqlNodeCache INSTANCE = new MetaSqlNodeCache();
    private final Map<Object, Object> nodes;

    private MetaSqlNodeCache() {
        this.nodes = new ConcurrentHashMap<>();
    }

    @Override
    public String getId() {
        return Constants.CACHE_KEY_SQL_NODE;
    }

    @Override
    public void putObject(Object key, Object value) {
        if(StringUtils.isEmpty(key)){
            return;
        }
        nodes.put(key, value);
    }

    @Override
    public Object getObject(Object key) {
        if(StringUtils.isEmpty(key)){
            return null;
        }
        return nodes.get(key);
    }

    @Override
    public Object removeObject(Object key) {
        if(StringUtils.isEmpty(key)){
            return null;
        }
        return nodes.remove(key);
    }

    @Override
    public void clear() {
        nodes.clear();
    }

    @Override
    public int getSize() {
        return nodes.size();
    }
}