/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.cache;

import com.je.ibatis.extension.metadata.model.Function;
import com.je.ibatis.extension.metadata.model.Table;
import com.je.ibatis.session.CustomConfiguration;

/**
 * 元数据缓存管理
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/7/24
 */
public interface MetaDataCacheManager {

    /**
     * 功能数据加入缓存
     *
     * @param func 功能数据
     */
    void put(Function func);

    /**
     * 资源表数据加入缓存
     *
     * @param table 资源表数据
     */
    void put(Table table);

    /**
     * 删除功能缓存
     *
     * @param code 功能编码
     */
    void removeFunction(String code);

    /**
     * 删除资源表缓存
     *
     * @param code 资源表编码
     */
    void removeTable(String code);

    /**
     * 缓存中获取功能数据
     *
     * @param code 功能编码
     * @return 功能数据
     */
    Function getFunction(String code);

    /**
     * 缓存中获取资源表数据
     *
     * @param code 资源表编码
     * @return 资源表数据
     */
    Table getTable(String code);

    /**
     * 清空全部缓存
     */
    void clear();

    /**
     * 清空全部功能缓存
     */
    void clearFunction();

    /**
     * 清空资源表缓存
     */
    void clearTable();

    /**
     * 设置 configuration
     *
     * @param configuration Mybatis配置
     */
    void setConfiguration(CustomConfiguration configuration);
}
