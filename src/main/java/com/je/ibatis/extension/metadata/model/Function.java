/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.metadata.model;

import java.util.List;

/**
 * Function
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/21
 */
public class Function extends BaseMeta {

    /**
     * 表名
     */
    private String code;

    /**
     * 表注释
     */
    private String name;

    /**
     * 对应的表
     */
    private String tableCode;

    /**
     * 功能加载项字段名集合
     */
    private List<String> loadColumnNames;

    /**
     * 功能对应表元数据
     */
    private Table table;

    public Function(String code, String tableCode, List<String> loadColumnNames) {
        this.code = code;
        this.tableCode = tableCode;
        this.loadColumnNames = loadColumnNames;
    }

    public Function(String code, String name, String tableCode, List<String> loadColumnNames) {
        this(code, tableCode, loadColumnNames);
        this.name = name;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public List<String> getLoadColumnNames() {
        return loadColumnNames;
    }

    public void setLoadColumnNames(List<String> loadColumnNames) {
        this.loadColumnNames = loadColumnNames;
    }

    @Override
    public String toString() {
        return "Function{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", tableCode='" + tableCode + '\'' +
                ", loadColumnNames=" + loadColumnNames +
                ", table=" + table +
                '}';
    }
}