/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.parse;

import com.je.ibatis.extension.metadata.model.Function;
import com.je.ibatis.extension.metadata.model.Table;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * 元数据解析
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/22
 */
public interface MetaDataParse {

    /**
     * 获取所有表元数据
     *
     * @return java.util.List<com.je.ibatis.extension.metadata.model.Table>
     */
    List<Table> tables();

    /**
     * 获取指定表元数据
     *
     * @return java.util.List<com.je.ibatis.extension.metadata.model.Table>
     */
    List<Table> tables(List<String> tableCodes);

    /**
     * 获取全部功能元数据
     *
     * @return java.util.List<com.je.ibatis.extension.metadata.model.Function>
     */
    List<Function> functions();

    /**
     * 获取指定表元数据
     *
     * @param tableCode 表名
     * @return com.je.ibatis.extension.metadata.model.Table
     */
    Table table(String tableCode);

    /**
     * 获取指定功能元数据
     *
     * @param functionCode 功能编码
     * @return Function
     */
    Function function(String functionCode);

    /**
     * 设置解析器数据源
     *
     * @param dataSource 数据源
     */
    void setDataSource(DataSource dataSource);

    /**
     * 格式化数据
     *
     * @param table 表信息
     * @param bean  操作的数据实体
     * @return 是否转换成功
     */
    boolean formData(Table table, Map<String, Object> bean);
}
