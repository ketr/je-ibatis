/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.enums;

import com.je.ibatis.extension.conditions.ISqlSegment;

/**
 * 条件关键字枚举
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/28
 */
public enum Conditions implements ISqlSegment {
    EMPTY(" "),
    WHERE(" WHERE "),
    AND("AND"),
    OR("OR"),
    IN("IN"),
    NOT("NOT"),
    LIKE("LIKE"),
    PERCENT("%"),
    EQ("="),
    NE("<>"),
    GT(">"),
    GE(">="),
    LT("<"),
    LE("<="),
    IS_NULL("IS NULL"),
    IS_NOT_NULL("IS NOT NULL"),
    LEFT_BRACKET("("),
    RIGHT_BRACKET(")"),
    COMMA(","),
    GROUP_BY("GROUP BY"),
    HAVING("HAVING"),
    ORDER_BY("ORDER BY"),
    EXISTS("EXISTS"),
    BETWEEN("BETWEEN"),
    ASC("ASC"),
    DESC("DESC"),
    SHOW("SHOW"),
    SELECT("SELECT"),
    DELETE("DELETE"),
    UPDATE("UPDATE"),
    INSERT("INSERT"),
    CREAT("CREATE"),
    ALTER("ALTER"),
    DROP("DROP"),
    COMMENT("COMMENT"),
    TRUNCATE("TRUNCATE"),
    IF("IF"),
    EXEC("EXEC"),
    EXECUTE("EXECUTE")
    ;

    private final String keyword;

    Conditions(final String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String getSqlSegment() {
        return keyword;
    }

    @Override
    public String toString() {
        return keyword;
    }
}
