/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.conditions;

import java.util.HashMap;

import static com.je.ibatis.extension.toolkit.Constants.WRAPPER_ALIAS;
import static com.je.ibatis.extension.toolkit.Constants.WRAPPER_PARAMETER;

/**
 * 参数包装类
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/28
 */
public class ParameterWrapper extends HashMap<String, Object> {

    /**
     * 添加参数
     *
     * @param value 参数
     * @return String 参数的key
     */
    public String put(Object value) {
        //生成参数key
        String key = WRAPPER_ALIAS + super.size();
        super.put(key, value);
        //生成预处理引用
        key = String.format(WRAPPER_PARAMETER, WRAPPER_ALIAS, key);
        return key;
    }

}