/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.ibatis.extension.mapper;

import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.ibatis.extension.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 元数据基础方法封装
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/11/22
 */
public interface MetaBaseMapper {

    /**
     * 平台资源表基础插入方法
     * <p>
     * 需在Map中加入tableCode或者funcCode
     * 执行插入后会在beanMap中加入自动生成的主键
     *
     * @param beanMap 要插入的数据
     * @return int
     */
    int insertMap(Map<String, Object> beanMap);

    /**
     * 平台资源表批量插入方法
     * <p>
     * 执行插入后会在map中加入自动生成的主键
     *
     * @param tableCode 表名
     * @param list      要插入的数据
     * @return int
     */
    int insertBatch(@Param(Constants.KEY_TABLE_CODE) String tableCode, @Param(Constants.LIST_ALIAS) List<Map<String, Object>> list);

    /**
     * 平台资源表基础修改方法
     * <p>
     * 需在Map中加入tableCode或者funcCode
     * 必须传入主键
     *
     * @param beanMap 要修改的数据
     * @param wrapper where条件
     * @return int
     */
    int updateMap(@Param(Constants.MAP_ALIAS) Map<String, Object> beanMap, @Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 平台资源表基础查询方法
     * <p>
     * 需在wrapper中加入tableCode或者funcCode
     * 查询表所有数据
     *
     * @param page    分页对象
     * @param wrapper 查询条件
     * @return List
     */
    List<Map<String, Object>> select(@Param(Constants.PAGE_ALIAS) Page page, @Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 平台功能基础查询方法
     * <p>
     * 需在wrapper中加入funcCode
     * 查询表所有数据
     *
     * @param page    分页对象
     * @param wrapper 查询条件
     * @return List
     */
    List<Map<String, Object>> load(@Param(Constants.PAGE_ALIAS) Page page, @Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 根据主键查询
     *
     * @param tableCode 表名
     * @param pkValue   主键值
     * @return Map
     */
    Map<String, Object> selectOneByPk(@Param(Constants.KEY_TABLE_CODE) String tableCode, @Param(Constants.KEY_PK_VALUE) String pkValue, @Param(Constants.KEY_SELECT_COLUMN) String columns);

    /**
     * 平台资源表基础查询方法
     * <p>
     * 需在wrapper中加入tableCode或者funcCode
     * 查询表所有数据
     *
     * @param page    分页对象
     * @param wrapper 查询条件
     * @return List
     */
    List<Map<String, Object>> selectSql(@Param(Constants.PAGE_ALIAS) Page page, @Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 平台资源表基础删除方法
     * <p>
     * 需在wrapper中加入tableCode
     *
     * @param wrapper 查询条件
     * @return int
     */
    int delete(@Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 执行新增sql
     *
     * @param wrapper 语句对象
     * @return int
     */
    int insertSql(@Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 执行更新sql
     *
     * @param wrapper 语句对象
     * @return int
     */
    int updateSql(@Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

    /**
     * 执行删除sql
     *
     * @param wrapper 语句对象
     * @return int
     */
    int deleteSql(@Param(Constants.WRAPPER_ALIAS) ConditionsWrapper wrapper);

}
